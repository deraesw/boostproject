﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Rocket : MonoBehaviour
{

    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 100f;

    Rigidbody rigidbody;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        Thrust();
        Rotate();
    }

    void OnCollisionEnter(Collision collision) {
        switch(collision.gameObject.tag) {
            case "Friendly": break;
            case "Finish":
                SceneManager.LoadScene(1);
                break;
            default:
                SceneManager.LoadScene(0);
                break;
        }
    }

    private void Thrust() {
        float thrustFrameSpeed = mainThrust * Time.deltaTime;
        if (Input.GetKey(KeyCode.Space)) {
            //Thrusting
            rigidbody.AddRelativeForce(Vector3.up * mainThrust);
            if (!audioSource.isPlaying) {
                audioSource.Play();

            }
        }
        else {
            if (audioSource.isPlaying) {
                audioSource.Stop();
            }
        }
    }

    private void Rotate() {
        rigidbody.freezeRotation = true;

        float rotateFrameSpeed = rcsThrust * Time.deltaTime;
        if (Input.GetKey(KeyCode.Q)) {
            transform.Rotate(Vector3.forward * rotateFrameSpeed);
        }
        else if (Input.GetKey(KeyCode.D)) {
            transform.Rotate(-Vector3.forward * rotateFrameSpeed);
        }

        rigidbody.freezeRotation = false;
    }
}
